import { HOME, TRENDING, FAVORITES, ABOUT_US, UPLOAD, CONTAINER_SELECTOR } from '../common/constants.js';
import { toAboutUsView } from '../views/about-us-view.js';
import { toTrendingView } from '../views/trending-view.js';
import { toHomeView } from '../views/home-view.js';
import { toUploadView } from '../views/upload-view.js';
import { q, setActiveNav } from './helpers.js';
import { loadTrendingGifs, loadHomeTrendingGifs, loadHomeFavorites } from '../requests/request-service.js';
import { getGifById } from '../data/giphy.js';
import { toFavoritesView } from '../views/favorites-view.js';
import { getUploaded } from '../data/uploaded.js';

// public API
export const loadPage = (page = '') => {
  switch (page) {
  case HOME:
    setActiveNav(HOME);
    return renderHome();
  case TRENDING:
    setActiveNav(TRENDING);
    return renderTrendingGifs();
  case FAVORITES:
    setActiveNav(FAVORITES);
    return renderFavorites();
  case ABOUT_US:
    setActiveNav(ABOUT_US);
    return renderAboutUs();
  case UPLOAD:
    setActiveNav(UPLOAD);
    return renderUpload();

    /* if the app supports error logging, use default to log mapping errors */
  default:
    return null;
  }
};

// private functions
const renderHome = async () => {
  const trending = await loadHomeTrendingGifs();

  const favorites = await loadHomeFavorites();
  let uploaded = await Promise.all(getUploaded().map(getGifById));

  uploaded = { data: uploaded.map(x => x.data) };

  q(CONTAINER_SELECTOR).innerHTML = toHomeView(trending, favorites, uploaded);
};

const renderTrendingGifs = async () => {
  const tr = await loadTrendingGifs(20);
  q(CONTAINER_SELECTOR).innerHTML = toTrendingView(tr);
};

const renderFavorites = async () => {
  const favorites = await loadHomeFavorites();
  q(CONTAINER_SELECTOR).innerHTML = toFavoritesView(favorites);
};

const renderAboutUs = () => {
  q(CONTAINER_SELECTOR).innerHTML = toAboutUsView();
};

const renderUpload = () => {
  q(CONTAINER_SELECTOR).innerHTML = toUploadView();
};
