import { getTrendingGifs, searchGifs, getGifsByIds, getRandomGif, } from '../data/giphy.js';
import { getFavorites } from '../data/favorites.js';

export const loadTrendingGifs = async (limit, offset) => {
  const result = await getTrendingGifs(limit, offset);
  return result;
};

export const getSearchResults = async (query, limit, offset) => {
  const result = await searchGifs(query, limit, offset);
  return result;
};

export const loadHomeTrendingGifs = async () => {
  const result = await getTrendingGifs(10);

  return result;
};

export const loadHomeFavorites = async () => {
  const favorites = getFavorites();

  if (!favorites.length)
  {
    const randomGif = await getRandomGif();
    favorites.push(randomGif.data.id);
  }
  
  return await getGifsByIds(favorites);
};