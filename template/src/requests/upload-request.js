import { API_KEY } from '../common/constants.js';
import { addUploaded, clearUploaded } from '../data/uploaded.js';
import { q } from '../events/helpers.js';
import { loadPage } from '../events/navigation-events.js';
import { HOME } from '../common/constants.js';

// eslint-disable-next-line consistent-return
export const uploadingGif = async () => {
  const form = q('#upload-form');
  const formData = new FormData(form);

  formData.append('api_key', API_KEY);
  if (formData.get('file').name === '')
    return alert('There is no image selected');
  try {
    q('.loader').style.visibility = 'visible';
    
    const response = await fetch('https://upload.giphy.com/v1/gifs', {
      method: 'POST',
      body: formData,
    });
    const response3 = await response.json();
    const uploadId = response3.data.id;
    addUploaded(uploadId);
    q('.loader').style.visibility = 'hidden';
    loadPage(HOME);

  } catch (error) {
    console.log(error);
  }
};

export const clearUploads = () => {
  clearUploaded();
};